<?php
$con = mysqli_connect('localhost', 'root', '', 'exam');
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="style.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<body>

    <div>
        <div class="Main">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                Add
            </button>

        </div>
        <div class="Table">
        <table>
            <table>
                <tr>
                    <th>Image Title</th>
                    <th>Thumbnail</th>
                    <th>File name</th>
                    <th>Date Added</th>
                    <th>Actions</th>
                </tr>
                <?php
                $data = mysqli_query($con, "SELECT * FROM images");
                while ($row = $data->fetch_assoc()) {
                    echo "
                    <tr>
                        <td>
                        " . $row['title'] . "
                        </td>
                        <td>
                        <img style='width:100px' src='img/" . $row['thumbnail'] . "'/>
                        </td>
                        <td>
                        " . $row['filename'] . "
                        </td>
                        <td>
                        " . $row['date_added'] . "
                        </td>
                        <td>
                        <a href='javascript:void(0)' onclick='editrow(" . json_encode($row) . ")' class = 'btn btn-success'>Edit</a>
                        <a href='javascript:void(0)' onclick='remove(" . json_encode($row) . ")' class = 'btn btn-danger'>Delete</a>
                        </td>
                    </tr>
                        ";
                }
                ?>

            </div>
            </table>
        </table>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal</h5>
                    <button type="button" id="modal-button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="insert.php" method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="exampleFormControlFile1">Add Image</label>
                            <input id="editthumbnail" type="file" class="form-control-file" name="thumbnail">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Image Title</label>
                            <input id="editimage" type="text" class="form-control" name="image_title">
                            <input id="id" type="text" name="id" hidden>
                        </div>
                        <button type="submit" name="upload" class="btn btn-primary">Save changes</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

</html>

<?php



$message = "";

if (isset($_POST['delete'])) {

    $ID = $_GET['id'];
}



?>
<script>
    function editrow(row) {
        console.log(row)
        $("#id").val(row.id)
        $("#editimage").val(row.title)
        $("#exampleModal").modal("show")
    }

    function remove(row){
        var conf=confirm("Are you sure?");
        if(conf==true){
        $.ajax({
            url:"delete.php",
            type:"POST",
            data:{
                id:row.id
            },
            success:function(){
                location.reload()
            }
        })
        }else{
            return false
        }

    }

    $("#exampleModal").on("hidden.bs.modal", function() {
        console.log("modalisclose")
        $("#id").val("")
        $("#editimage").val("")
    })
</script>
